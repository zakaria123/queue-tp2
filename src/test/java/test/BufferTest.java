package test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.PrintStream;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;
import ca.qc.claurendeau.storage.Buffer;
import ca.qc.claurendeau.storage.Element;

public class BufferTest {

	Buffer buffer;
	int capacity;
	Element element;
	String message;
	PrintStream printStream;
	ByteArrayOutputStream baos;

	@Before
	public void setUp() {
		capacity = 5;
		element = new Element();
		element.setData(3);
		buffer = new Buffer(capacity);
		baos = new ByteArrayOutputStream();
		printStream = new PrintStream(baos);
	}

	@Test
	public void getCapacityTest() {
		assertThat(buffer.capacity()).isEqualTo(capacity);
	}

	@Test
	public void isEmptyTest() {
		assertTrue(buffer.isEmpty());
	}

	@Test
	public void addElementTest1() throws BufferFullException {
		buffer.addElement(element);
		assertFalse(buffer.isEmpty());
	}

	@Test
	public void isFullTest() throws BufferFullException {
		for (int i = 0; i < 5; i++) {
			buffer.addElement(element);
		}
		assertTrue(buffer.isFull());
	}

	@Test(expected = BufferFullException.class)
	public void addElementTest2() throws BufferFullException {
		for (int i = 0; i < 7; i++) {
			element.setData(i);
			buffer.addElement(element);
		}
	}

	@Test(expected = RuntimeException.class)
	public void addElementTest3() throws BufferFullException {
		buffer.addElement(null);
	}

	@Test
	public void removeElementTest1() throws BufferFullException, BufferEmptyException {
		buffer.addElement(new Element());
		buffer.removeElement();
		assertThat(buffer.isEmpty()).isTrue();
	}

	@Test(expected = BufferEmptyException.class)
	public void removeElementTest2() throws BufferEmptyException {
		buffer.removeElement();
	}

	@Test
	public void toStringTest() throws BufferFullException, IOException {
		Element element2 = new Element();
		element2.setData(4);
		buffer.addElement(element2);
		buffer.addElement(element);
		printStream.println(buffer.toString());
		assertEquals(element2.toString() + "," + element.toString() + ",", baos.toString().trim());
		uncaptureSystemOut();
	}

	@Test
	public void bufferEmptyExceptionTest1() {
		try {
			throw new BufferEmptyException();
		} catch (BufferEmptyException e) {
			printStream.println(e.getMessage());
			assertEquals(baos.toString().trim(), BufferEmptyException.message);
			uncaptureSystemOut();
		}
	}

	@Test
	public void bufferEmptyExceptionTest2() {
		message = "vous avez retir� un �l�ment de trop dans la collection";
		try {
			throw new BufferEmptyException(message);
		} catch (BufferEmptyException e) {
			printStream.println(e.getMessage());
			assertEquals(baos.toString().trim(), message);
			uncaptureSystemOut();
		}
	}

	@Test
	public void bufferFullExceptionTest1() {
		try {
			throw new BufferFullException();
		} catch (BufferFullException e) {
			printStream.println(e.getMessage());
			assertEquals(baos.toString().trim(), BufferFullException.message);
			uncaptureSystemOut();
		}
	}

	@Test
	public void bufferFullExceptionTest2() {
		message = "vous avez rajouter un �l�ment de trop dans la collection";
		try {
			throw new BufferFullException(message);
		} catch (BufferFullException e) {
			printStream.println(e.getMessage());
			assertEquals(baos.toString().trim(), message);
			uncaptureSystemOut();
		}
	}

	private void uncaptureSystemOut() {
		System.out.flush();
		System.setOut(printStream);
	}

	@After
	public void tearDown() throws IOException {
		buffer = null;
		capacity = 0;
		element = null;
		baos = null;
		printStream = null;
	}
}
