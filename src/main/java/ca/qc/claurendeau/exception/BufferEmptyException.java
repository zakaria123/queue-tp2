package ca.qc.claurendeau.exception;

public class BufferEmptyException extends Exception {

	public final static String message = "Erreur, car vous avez la collection vide";

	public BufferEmptyException(String message) {
		super(message);
	}

	public BufferEmptyException() {
		this(message);
	}
}
