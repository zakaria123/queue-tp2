package ca.qc.claurendeau.exception;

public class BufferFullException extends Exception {
	public final static String message = "Erreur, car vous avez atteint le maximum";

	public BufferFullException(String message) {
		super(message);
	}

	public BufferFullException() {
		this(message);
	}
}
