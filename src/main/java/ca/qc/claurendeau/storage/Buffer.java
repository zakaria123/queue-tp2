package ca.qc.claurendeau.storage;

import ca.qc.claurendeau.exception.BufferEmptyException;
import ca.qc.claurendeau.exception.BufferFullException;

public class Buffer {
	private int capacity;
	private int currentLoad;
	private Element premier;
	private Element dernier;

	public Buffer(int capacity) {
		this.capacity = capacity;
	}

	public String toString() {
		Element element = this.premier;
		String string = "";
		while (element != null) {
			string += element + ",";
			element = element.getProchain();
		}
		return string;
	}

	public int capacity() {
		return this.capacity;
	}

	public int getCurrentLoad() {
		return this.currentLoad;
	}

	public boolean isEmpty() {
		return this.currentLoad == 0;
	}

	public boolean isFull() {
		return this.capacity == currentLoad;
	}

	public synchronized void addElement(Element element) throws BufferFullException {
		if (element == null) {
			throw new RuntimeException();
		}
		if (isFull()) {
			throw new BufferFullException();
		}
		if (isEmpty()) {
			this.premier = element;
			this.dernier = this.premier;
		} else {
			this.dernier.setProchain(element);
			this.dernier = this.dernier.getProchain();
		}
		this.currentLoad++;
	}

	public synchronized Element removeElement() throws BufferEmptyException {
		if (isEmpty()) {
			throw new BufferEmptyException();
		}
		Element element = this.premier;
		this.premier = this.premier.getProchain();
		this.currentLoad--;
		return element;
	}
}
