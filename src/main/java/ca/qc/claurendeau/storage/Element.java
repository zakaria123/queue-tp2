package ca.qc.claurendeau.storage;

public class Element {
	private int data;
	private Element prochainElement;

	public void setData(int data) {
		this.data = data;
	}

	public int getData() {
		return data;
	}

	public Element getProchain() {
		return prochainElement;
	}

	public void setProchain(Element element) {
		this.prochainElement = element;
	}

	@Override
	public String toString() {
		return "data=" + data;
	}

}
